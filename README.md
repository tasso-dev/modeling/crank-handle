# Highly Parametric Crank

[![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)


# Modules

## crank_arm

<img align="left" alt="crank_arm isometric view" src="docs/images/crank-arm/crank_arm_fig1.png" width="800" height="600">

<br clear="all" />

### Description: 

Generates the arm portion of a crank.

The arm is comprised of 3 major sub-assemblies; the bore housing, which
contains the bore that attaches to the shaft this crank will turn; the handle housing, which attaches the crank
handle which is used to turn this arm, and a bridge which connects the two housings.

Each sub-component is further broken down into additional sub-assemblies. The bore housing is comprised of a
primary housing, and a sub-housing, which attaches the bridge to the bore housing. The handle housing is comprised
of a primary housing, and a sub-housing, which is used to separate the arm and the handle

### Arguments: 

<abbr title="These args can be used by position or by name.">By&nbsp;Position</abbr> | What it does
-------------------- | ------------
`bore_diameter`      | the diamater (in mm) of the bore
`bore_length`        | the length (in mm) of the bore, and the height of the bore housing
`bore_housing_length` | the length (in mm) of the housing which contains the bore
`bore_housing_width` | the width (in mm) of the housing which contains the bore
`set_screw_diameter` | the diameter (in mm) of the opening which contains the set screw
`set_screw_z_offset` | the distance (in mm) from the bottom of the bore housing and the center of the set screw hold
`bore_sub_housing_height` | the height (in mm) of the sub-housing which connects the bore housing to the bridge
`bridge_length`      | the length (in mm) of the bridge which connects the bore housing and handle housings
`handle_housing_length` | the length (in mm) of the housing to which the handle attaches
`handle_housing_width` | the width (in mm) of the housing to which the handle attaches
`handle_housing_height` | the height (in mm) of the housing to which the handle attaches
`handle_housing_z_offset` | The distance (in mm) between the bottom of the bore housing and the bottom of the handle
`housing`           
`handle_sub_housing_diameter` | The diameter (in mm) of the sub-housing which separates the handle housing and the
`handle`            
`handle_sub_housing_height` | The height (in mm) of the sub-housing which separates the handle housing and the
`handle`            
`handle_bolt_diameter` | The diameter (in mm) of the shaft of the bolt which attaches the handle to the crank arm
`handle_bolt_head_diameter` | The diameter (in mm) of opposing vertices of the opening containing the hex bolt head
`handle_bolt_head_height` | The height (in mm) of the opening containing the hex bolt head
`fillet_radius`      | The radius (in mm) of the fillet to be applied to this crank arm

### Figures

**Figure 2:** Top

<img align="left" alt="crank_arm top view" src="docs/images/crank-arm/crank_arm_fig2.png" width="800" height="600">

<br clear="all" />

**Figure 3:** Side

<img align="left" alt="crank_arm side view" src="docs/images/crank-arm/crank_arm_fig3.png" width="800" height="600">

<br clear="all" /> 

**Figure 4:** Bottom

<img align="left" alt="crank_arm top view" src="docs/images/crank-arm/crank_arm_fig4.png" width="800" height="600">

<br clear="all" /> 


##  crank_handle

<img align="left" alt="crank_handle isometric view" src="docs/images/crank-handle/crank_handle_fig1.png" width="800" height="600">

<br clear="all" />

### Description: 

Generates the handle portion of a crank.

The handle is comprised of the main shaft, which can be tapered according the bottom and top widths, a rounded cap,
and a recess/hole within which the bolt attaching the handle to the arm resides.

### Arguments: 

<abbr title="These args can be used by position or by name.">By&nbsp;Position</abbr> | What it does
-------------------- | ------------
`length`             | the overall length (in mm) of the handle, from the bottom of the base to the top of the cap
`bottom_width`       | the width (in mm) of the base of the handle
`top_width`          | the width (in mm) of the top of the handle
`bolt_diameter`      | the diameter (in mm) of the bolt which attaches this handle to the crank arm
`bolt_recess_depth`  | the depth (in mm) of the recess in which the bolt resides
`bolt_recess_depth`  | the diameter (in mm) of the recess in which the bold resides
`cap_height`         | the height (in mm) of the rounded cap of the handle

### Figures

**Figure 2:** Top

<img align="left" alt="crank_handle top view" src="docs/images/crank-handle/crank_handle_fig2.png" width="800" height="600">

<br clear="all" />

**Figure 3:** Side

<img align="left" alt="crank_handle side view" src="docs/images/crank-handle/crank_handle_fig3.png" width="800" height="600">

<br clear="all" />

**Figure 4:** Bottom

<img align="left" alt="crank_handle bottom view" src="docs/images/crank-handle/crank_handle_fig4.png" width="800" height="600">

<br clear="all" />


# License

## Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

Copyright © 2023 Andrew A. Tasso

This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International 
(CC BY-NC-SA 4.0) License. The full terms of the license can be found in [LICENSE.txt](LICENSE.txt) in the root of this 
project or at http://creativecommons.org/licenses/by-nc-sa/4.0/.


## Third Party

This project utilizes 3rd party libraries that are distributed under their own terms. For a complete list of libraries
and coordinating licenses, see [LICENSE-3RD-PARTY.txt](LICENSE-3RD-PARTY.txt) in the root of this project.

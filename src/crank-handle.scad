/*
* Highly Parametric Crank (c) by Andrew A. Tasso
* 
* Highly Parametric Crank is licensed under a Creative Commons 
* Attribution-NonCommercial-ShareAlike 4.0 International License.
* 
* You should have received a copy of the license along with this
* work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
*/

// File: crank-handle.scad
// FileGroup: Highly Parametric Crank
// FileSummary: Module to generate the handle portion of a crank.
// Includes:
//   use <crank-handle.scad>

use <./lib/dotSCAD/rounded_cylinder.scad>

// keep the number of fragments low while in preview to improve performance
$fn = $preview ? 20 : 100;

eps = 0.001;

//TODO Add customizer variables

crank_handle();

// Module: crank_handle
// Usage:
//   crank_handle();
// Description:
//   Generates the handle portion of a crank.
//   .
//   The handle is comprised of the main shaft, which can be tapered according the bottom and top widths, a rounded cap,
//   and a recess/hole within which the bolt attaching the handle to the arm resides.  
// Arguments:
//   length = the overall length (in mm) of the handle, from the bottom of the base to the top of the cap
//   bottom_width = the width (in mm) of the base of the handle
//   top_width = the width (in mm) of the top of the handle
//   bolt_diameter = the diameter (in mm) of the bolt which attaches this handle to the crank arm
//   bolt_recess_depth = the depth (in mm) of the recess in which the bolt resides
//   bolt_recess_depth = the diameter (in mm) of the recess in which the bold resides
//   cap_height = the height (in mm) of the rounded cap of the handle
// Figure(VPT=[0,0,40], VPR=[55,0,25], VPD=225 NoAxes, Huge, Render, ColorScheme=Nature): Isometric
//   crank_handle();
// Figure(VPT=[0,0,0], VPR=[0,0,0], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Top
//   crank_handle();
// Figure(VPT=[0,0,39], VPR=[90,0,0], VPD=225 NoAxes, Huge, Render, ColorScheme=Nature): Side
//   crank_handle();
// Figure(VPT=[0,0,0], VPR=[180,0,0], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Bottomr
//   crank_handle();
module crank_handle(length = 78, bottom_width = 21, top_width = 29, bolt_diameter = 8.5, bolt_recess_depth = 15, 
                    bolt_recess_diameter = 19, cap_height = 5) {

    difference() {

        handle_body(length = length, bottom_width = bottom_width, top_width = top_width);
        
        bolt_hole(
            length = length, 
            bolt_diameter = bolt_diameter, 
            recess_depth = bolt_recess_depth, 
            recess_diameter = bolt_recess_diameter
        );

    }

    module handle_body(length, bottom_width, top_width, cap_height=5) {

        union() {

            cylinder(h = length - cap_height, d1 = bottom_width, d2 = top_width);

            translate(v = [0, 0, length - (2 * cap_height)]) {

                difference() {
                
                    rounded_cylinder(radius = top_width / 2, h = cap_height * 2, round_r = cap_height);
                
                    translate(v = [0, 0, -eps])
                        cylinder(h = cap_height + eps, d = top_width);
                
                }
            }
        }
    }

    module bolt_hole(length, bolt_diameter, recess_depth, recess_diameter = 19) {

        translate(v = [0, 0, length - recess_depth])
            cylinder(h = recess_depth + eps, d = recess_diameter);

        translate(v = [0, 0, -eps])
            cylinder(h = length, d = bolt_diameter);

    }
}
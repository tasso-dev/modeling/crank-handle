/*
* Highly Parametric Crank (c) by Andrew A. Tasso
* 
* Highly Parametric Crank is licensed under a Creative Commons 
* Attribution-NonCommercial-ShareAlike 4.0 International License.
* 
* You should have received a copy of the license along with this
* work. If not, see <http://creativecommons.org/licenses/by-nc-sa/4.0/>.
*/

// File: crank-arm.scad
// FileGroup: Highly Parametric Crank
// FileSummary: Module to generate the arm portion of a crank.
// Includes:
//   use <crank-arm.scad>

use <./lib/dotSCAD/bezier_curve.scad>
use <./lib/dotSCAD/rounded_cube.scad>
use <./lib/dotSCAD/rounded_cylinder.scad>
use <./lib/dotSCAD/shape_trapezium.scad>
use <./lib/dotSCAD/sweep.scad>
use <./lib/dotSCAD/rounded_square.scad>

// keep the number of fragments low while in preview to improve performance
$fn = $preview ? 20 : 100;

//TODO Add customizer variables

eps = 0.001;

crank_arm();

// Module: crank_arm
// Usage:
//   crank_arm();
// Description:
//   Generates the arm portion of a crank. 
//   .
//   The arm is comprised of 3 major sub-assemblies; the bore housing, which 
//   contains the bore that attaches to the shaft this crank will turn; the handle housing, which attaches the crank 
//   handle which is used to turn this arm, and a bridge which connects the two housings.
//   .
//   Each sub-component is further broken down into additional sub-assemblies. The bore housing is comprised of a 
//   primary housing, and a sub-housing, which attaches the bridge to the bore housing. The handle housing is comprised 
//   of a primary housing, and a sub-housing, which is used to separate the arm and the handle
// Arguments:
//   bore_diameter = the diamater (in mm) of the bore 
//   bore_length = the length (in mm) of the bore, and the height of the bore housing
//   bore_housing_length = the length (in mm) of the housing which contains the bore
//   bore_housing_width = the width (in mm) of the housing which contains the bore
//   set_screw_diameter = the diameter (in mm) of the opening which contains the set screw
//   set_screw_z_offset = the distance (in mm) from the bottom of the bore housing and the center of the set screw hold
//   bore_sub_housing_height = the height (in mm) of the sub-housing which connects the bore housing to the bridge
//   bridge_length = the length (in mm) of the bridge which connects the bore housing and handle housings
//   handle_housing_length = the length (in mm) of the housing to which the handle attaches
//   handle_housing_width = the width (in mm) of the housing to which the handle attaches
//   handle_housing_height = the height (in mm) of the housing to which the handle attaches
//   handle_housing_z_offset = The distance (in mm) between the bottom of the bore housing and the bottom of the handle 
//   housing
//   handle_sub_housing_diameter = The diameter (in mm) of the sub-housing which separates the handle housing and the 
//   handle
//   handle_sub_housing_height = The height (in mm) of the sub-housing which separates the handle housing and the 
//   handle
//   handle_bolt_diameter = The diameter (in mm) of the shaft of the bolt which attaches the handle to the crank arm
//   handle_bolt_head_diameter = The diameter (in mm) of opposing vertices of the opening containing the hex bolt head
//   handle_bolt_head_height = The height (in mm) of the opening containing the hex bolt head
//   fillet_radius = The radius (in mm) of the fillet to be applied to this crank arm
// Figure(VPT=[50,15,25], VPR=[55,0,25], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Isometric
//   crank_arm();
// Figure(VPT=[50,15,100], VPR=[0,0,0], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Top
//   crank_arm();
// Figure(VPR=[90,0,0], VPT=[50,0,22], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Side
//   crank_arm();
// Figure(VPT=[50,15,100], VPR=[180,0,0], VPD=200 NoAxes, Huge, Render, ColorScheme=Nature): Bottom
//   crank_arm();
module crank_arm(bore_diameter = 18, bore_length = 23, bore_housing_length = 32, bore_housing_width = 30, 
                 set_screw_diameter = 6, set_screw_z_offset = 14, bore_sub_housing_height = 15, 
                 bridge_length = 33, handle_housing_length = 48, handle_housing_width = 26, 
                 handle_housing_height = 13, handle_housing_z_offset = 28.5, handle_sub_housing_diameter = 19, 
                 handle_sub_housing_height = 3, handle_bolt_diameter = 8.5, handle_bolt_head_diameter = 15, 
                 handle_bolt_head_height = 6, fillet_radius = 1.5) {

    bore_housing(
        bore_diameter = bore_diameter, 
        bore_length = bore_length, 
        housing_length = bore_housing_length,
        housing_width = bore_housing_width,
        set_screw_z_offset = set_screw_z_offset,
        set_screw_diameter = set_screw_diameter,
        sub_housing_height = bore_sub_housing_height,
        fillet_radius = fillet_radius
    );

    bore_sub_housing_z_offset = bore_length - bore_sub_housing_height;
    bridge_end_bottom_height = handle_housing_z_offset - bore_sub_housing_z_offset;

    translate(v = [bore_housing_length, bore_housing_width / 2, bore_sub_housing_z_offset])
        arm_bridge(
            length = bridge_length,
            end_bottom_height = bridge_end_bottom_height,
            start_width = bore_housing_width,
            start_height = bore_sub_housing_height,
            end_width = handle_housing_width,
            end_height = handle_housing_height,
            fillet_radius = fillet_radius
        );
    
    
    translate(v = [bore_housing_length + bridge_length, bore_housing_width / 2, handle_housing_z_offset]) 
        handle_housing(
            length = handle_housing_length,
            width = handle_housing_width,
            height = handle_housing_height, 
            sub_housing_diameter = handle_sub_housing_diameter,
            sub_housing_height = handle_sub_housing_height, 
            bolt_threading_diameter = handle_bolt_diameter,
            bolt_head_diameter = handle_bolt_head_diameter,
            bolt_head_height = handle_bolt_head_height
        );

    module bore_housing(bore_length, bore_diameter, housing_length, housing_width, set_screw_z_offset, 
                        set_screw_diameter, sub_housing_height, fillet_radius = 1.5) {


        translate(v = [housing_width / 2, housing_width / 2, 0]) 
            difference() {

                union() {

                    main_housing(width = housing_width, height = bore_length, fillet_radius = fillet_radius);

                    translate(v = [0, 0, bore_length - sub_housing_height]) 
                        sub_housing(length = housing_length - (housing_width / 2), 
                                    width = housing_width, 
                                    height = sub_housing_height, 
                                    fillet_radius = fillet_radius);

                }

                bore_hole(diameter = bore_diameter, height = bore_length);

                translate(v = [0, 0, set_screw_z_offset]) 
                    set_screw_hole(diameter = set_screw_diameter, length = (housing_width / 2) + eps);

            }

        module main_housing(width, height, fillet_radius = 1.5) {

            rounded_cylinder(radius = width / 2, h = height, round_r = fillet_radius);

        }

        module sub_housing(length, width, height, fillet_radius = fillet_radius) {
            
            translate(v = [0, -(width / 2), height])
                rotate(a = [0, 90, 0])
                linear_extrude(height = length) 
                rounded_square(size = [height, width], corner_r = fillet_radius);

        }
        
        module bore_hole(diameter, height) {

            translate(v = [0, 0, -eps])
                cylinder(h = height + 2 * eps, d = diameter);

        }

        module set_screw_hole(diameter, length) {

            rotate(a = [90, 0, 0])
                cylinder(h = length, d = diameter);

        }
    }

    module arm_bridge(length, end_bottom_height, start_width, start_height, end_width, end_height, fillet_radius = 1.5) {

        //The amount this sub-module needs to be moved to place the bottom of the start of the bridge at Z origin.
        bridge_start_z_offset = start_height / 2;

        //Since the bridge can arbitrarily scale in either x and/or y direction. Each segment of the bridge is centered 
        //to simplify alignment.
        start_center_coords = [0,0,0];
        end_center_coords = [length, 0, (end_bottom_height + (end_height / 2)) - bridge_start_z_offset];
        middle_center_coords = [length * .375, 0, end_center_coords[2] * .875];

        y_scale_factor = (start_width - end_width) / $fn;
        z_scale_factor = (start_height - end_height) / $fn;

        path_pts = bezier_curve(t_step = 1.0 / $fn, 
                                points = [start_center_coords, middle_center_coords, end_center_coords]);

        squares = [
            for(i = [0 : len(path_pts) - 1]) [             
                for(currPoint = shape_trapezium(length = start_width - (i * y_scale_factor), h = start_height - (i * z_scale_factor), corner_r = 1.5)) [
                    //shape_trapezium generates points coplanar to the z-axis origin, width on the x-axis, and height 
                    //on the y-axis. Expand the bridge along the z axis.
                    currPoint.x + path_pts[i][1], currPoint.y + path_pts[i][2], path_pts[i][0]
                ]
            ]
        ];

        translate(v = [0, 0, bridge_start_z_offset]) 
            rotate(a = [90,0,90])
            sweep(squares);

    }

    module handle_housing(length, width, height, sub_housing_diameter, sub_housing_height, bolt_threading_diameter, 
                          bolt_head_diameter, bolt_head_height, fillet_radius = 1.5) {

        difference() {

            union() {

                main_housing(length = length, width = width, height = height, fillet_radius = fillet_radius);

                translate(v = [length - width, 0, height - fillet_radius])
                    sub_housing(diameter = sub_housing_diameter, height = sub_housing_height, fillet_radius = fillet_radius);

            }

            translate(v = [length - width, 0, 0-eps])
                bolt_hole(length = height + sub_housing_height + (2 * eps), 
                          diameter = bolt_threading_diameter, 
                          head_height = bolt_head_height, 
                          head_diameter = bolt_head_diameter);

            sub_housing_surface_width = (sub_housing_diameter - bolt_threading_diameter - (fillet_radius * 2)) / 2;

            translate(v = [length - width, 0, height + sub_housing_height - .5])
                circle_text(diameter = bolt_threading_diameter + (sub_housing_surface_width / 2), 
                            text_size = sub_housing_surface_width / 2,
                            chars = " TASSO.DEV"); 

        }

        module main_housing(length, width, height, fillet_radius = 1.5) {

                union() {

                    translate(v = [0, -(width / 2), height])
                        rotate(a = [0, 90, 0])
                        linear_extrude(height = length - width) 
                        rounded_square(size = [height, width], corner_r = fillet_radius);

                    translate(v = [length - width, 0, 0])
                        rounded_cylinder(h = height, radius = width / 2, round_r = fillet_radius);

                }
        }

        module sub_housing(diameter, height, fillet_radius = 1.5) {

            rounded_cylinder(h = sub_housing_height + fillet_radius, 
                    radius = diameter / 2, 
                    round_r = fillet_radius);
          
        }

        module bolt_hole(length, diameter, head_height, head_diameter) {
          
            union() {

                //Head (Facet count of 6 ensures a hexagonal shape)
                cylinder(h = head_height, d = head_diameter, $fn = 6);
                //Shank/Threading
                cylinder(h = length, d = diameter);
                
            };
        }

        module circle_text(diameter, text_size, chars) {

            num_chars = len(chars);

            for(i = [0 : num_chars - 1]) {
                rotate(-i * (360 / num_chars)) 
                    translate(v = [0, diameter / 2 , 0]) 
                    linear_extrude(height = 1) 
                    text(text = chars[i], size = text_size, font = "Liberation Sans:style=Bold", halign = "center");
            }
            
        }
    }
}
